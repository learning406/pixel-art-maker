// add note code and event
var note = document.querySelector("#note");
var app = document.querySelector("#app");

resize();
window.onresize = resize;

function resize () {
  if (window.innerWidth < window.innerHeight) {
    note.classList.remove("hidden");
    app.classList.add("hidden");
  } else {
    note.classList.add("hidden");
    app.classList.remove("hidden");
  }
}

// background colors
var backColors = ['#ffffff', '#cccccc'];
var counter = 0;

// Select color input
var colorPicker = document.querySelector('#colorPicker');
var color = colorPicker.value;

// Select size input
var rows = document.querySelector('#inputHeight');
var columns = document.querySelector('#inputWidth');
var cells = document.querySelector('#inputSize');
var height = rows.value;
var width = columns.value;
var size = cells.value;

// select style tag
var sheet = document.querySelector('style');

// select canvas table
var table = document.querySelector('#pixelCanvas');

// call make grid function
makeGrid(height, width, size);

// When size is submitted by the user, call makeGrid()
var submit = document.querySelector('form');
submit.addEventListener('submit', function (event) {
  event.preventDefault();
  if (confirm("are you sure you want to resize \nyou will lose your work")) {
    height = rows.value;
    width = columns.value; 
    size = cells.value;
    makeGrid(height, width, size);
  }
});

// when clear is clicked
var clear = document.querySelector('button');
clear.addEventListener('click', function (event) {
  event.preventDefault();
  if (confirm("are you sure you want to clear workspace \nyou will lose your work")) {
    makeGrid(height, width, size);
    clear.style.visibility = 'hidden';
  }
});

function makeGrid(height, width, size) {
  table.innerHTML = '';
  sheet.innerHTML = `tr {height: ${size}px;} td {width: ${size}px;}`;
  counter = 0;
  
  for (var row = 1; row <= height; row++) {
    var newRow = document.createElement("tr");
    table.appendChild(newRow);

  	for (var column = 1; column <= width; column++) {
      counter++;
  		var newCell = document.createElement("td");
      if (counter%2===0)
        newCell.style.backgroundColor = backColors[0];
      else
        newCell.style.backgroundColor = backColors[1];
    	newRow.appendChild(newCell);
  	}
    
    if (columns.value%2===0)
      counter++;
  }
}

// set isDown flag
var isDown = false;
document.onmouseup = function () {
  isDown = false;
};

// set draw listeners
table.addEventListener('mousedown', function (event) {
  isDown = true;
  clear.style.visibility = 'visible';
  draw(event);
});

table.addEventListener('mouseover', function (event) {
  draw(event);
});

table.addEventListener('touchstart', function (event) {
  isDown = true;
  clear.style.visibility = 'visible';
  draw(event);
});

table.addEventListener('touchmove', function (event) {
  draw(event);
});

// set draw function
function draw(event) {
  console.log(event);
  if (event.target.nodeName == 'TD' && isDown) {
    color = colorPicker.value;
    event.target.style.backgroundColor = color;
  }
}

// change color function 
function changeColor(choosedColor) {
  colorPicker.value = choosedColor;
}